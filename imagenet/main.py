import argparse
import os
import shutil
import time
import datetime

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

from module.SparseDataParallel import SparseDataParallel
from module.SparseSGD import SparseSGD
from module.utils import AverageMeter, Logger, get_alg_id


model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))


parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('data', metavar='DIR',
                    help='path to dataset')
parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet18',
                    choices=model_names,
                    help='model architecture: ' +
                        ' | '.join(model_names) +
                        ' (default: resnet18)')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--epochs', default=90, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument('--lr', '--learning-rate', dest='lr', default=0.1, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--mt', '--momentum', dest='momentum', default=0.9,
                    type=float, metavar='M', help='momentum')
parser.add_argument('--wd', '--weight-decay', dest='weight_decay', default=1e-4,
                    type=float, metavar='WD', help='weight decay (default: 1e-4)')
parser.add_argument('--pf', '--print-freq', dest='print_freq', default=10,
                    type=int, metavar='T', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('-s','--sparsity', dest='sparsity', default=0.0, metavar='K',
                    type=float, help='sparsity of gradients')
parser.add_argument('-g', '--sparseAlgorithm', dest='algorithm', default='simple',
                    choices=['simple', 'accumulate', 'accumulate_momentum'],
                    help='algorithm for pruning gradients')
parser.add_argument('-w', '--warm-up', dest='is_warm_up', default=False,
                    action='store_true', help='whether warm up')
parser.add_argument('--ws', '--warm-up-step', dest='warm_up_step', default=0,
                    type=int, metavar='N', help='warm up step epochs/(minus) iterations')
parser.add_argument('--we', '--warm-up-end', dest='warm_up_end', default=0,
                    type=int, metavar='N', help='warm up ending epochs/(minus) iterations')
parser.add_argument('--wc','--warm-up-coeff', dest='warm_up_coeff', default=-1.0,
                    type=float, metavar='C', help='warm up coefficient')
parser.add_argument('-p', '--prune-all', dest='is_prune_root_gpu', default=False,
                    action='store_true', help='whether prune the root GPU')
parser.add_argument('-n', '--nGPU', dest='nGPU', default=4, type=int,
                    metavar='N', help='number of GPUs to use')
parser.add_argument('--sample', dest='is_sample', default=False,
                    action='store_true', help='whether using sample to get threshold')
parser.add_argument('--sample-rate', dest='sample_rate', default=-1,
                    metavar='K', type=float, help='sparsity of gradients')
parser.add_argument('-l', '--log-sparsity', dest='is_log_sparsity', default=False,
                    action='store_true', help='whether log communication sparsity')
parser.add_argument('--log-freq', dest='is_log_frequency', default=False,
                    action='store_true', help='whether log communication frequency')


best_prec1 = 0


def main():
    global args, best_prec1, train_log, test_log, checkpoint_dir
    args = parser.parse_args()
    # set correction coefficient
    vars(args)['correction_coeff'] = args.momentum

    dir_name, config = get_alg_id(**vars(args))
    log_dir = os.path.join('logs', dir_name)
    checkpoint_dir = os.path.join('checkpoints', dir_name)
    os.makedirs(log_dir)
    os.makedirs(checkpoint_dir)
    train_log = Logger(os.path.join(log_dir, 'train.log'))
    test_log = Logger(os.path.join(log_dir, 'test.log'))
    with open(os.path.join(log_dir, 'config.log'), 'w') as f:
        f.write(config)
    vars(args)['log_dir'] = log_dir

    # create model
    if args.pretrained:
        print("=> using pre-trained model '{}'".format(args.arch))
        model = models.__dict__[args.arch](pretrained=True)
    else:
        print("=> creating model '{}'".format(args.arch))
        model = models.__dict__[args.arch]()

    if args.arch.startswith('alexnet') or args.arch.startswith('vgg'):
        model.features = SparseDataParallel(module=model.features,
                                            device_ids=list(range(args.nGPU)),
                                            **vars(args))
        model.cuda()
    else:
        model = SparseDataParallel(module=model, device_ids=list(range(args.nGPU)),
                                   **vars(args)).cuda()

    # define loss function (criterion) and optimizer
    criterion = nn.CrossEntropyLoss().cuda()

    optimizer = SparseSGD(model.parameters(), args.lr,
                          momentum=args.momentum,
                          weight_decay=args.weight_decay)
    if args.algorithm == 'accumulate_momentum':
        optimizer.set_pruned_indices(model.get_pruned_indices())
    if args.is_log_frequency:
        optimizer.set_frequency_meter(model.get_frequency_meter())

   # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True

    # Data loading code
    traindir = os.path.join(args.data, 'train')
    valdir = os.path.join(args.data, 'val')
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(traindir, transforms.Compose([
            transforms.RandomSizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(valdir, transforms.Compose([
            transforms.Scale(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    if args.evaluate:
        validate(val_loader, model, criterion)
        return

    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch)

        # evaluate on validation set
        prec1 = validate(val_loader, model, criterion)

        # remember best prec@1 and save checkpoint
        is_best = prec1 > best_prec1
        best_prec1 = max(prec1, best_prec1)
        save_checkpoint({
            'epoch': epoch + 1,
            'arch': args.arch,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
            'optimizer' : optimizer.state_dict(),
        }, is_best)


def train(train_loader, model, criterion, optimizer, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    if args.warm_up_end > 0:
        model.warm_up(epoch_or_iter=epoch)
    # switch to train mode
    model.train()

    end = time.time()
    for i, (input, target) in enumerate(train_loader):
        if args.warm_up_end < 0:
            model.warm_up(epoch_or_iter=epoch * len(train_loader) + i)

        # measure data loading time
        data_time.update(time.time() - end)

        target = target.cuda(async=True)
        input_var = torch.autograd.Variable(input)
        target_var = torch.autograd.Variable(target)

        # compute output
        output = model(input_var)
        loss = criterion(output, target_var)

        # measure accuracy and record loss
        prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
        losses.update(loss.data[0], input.size(0))
        top1.update(prec1[0], input.size(0))
        top5.update(prec5[0], input.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        if args.algorithm == 'accumulate_momentum':
            optimizer.set_momentum_corrections(model.get_momentum_corrections())
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time,
                   data_time=data_time, loss=losses, top1=top1, top5=top5))
    train_log.write('%.4e\t%.4e\t%.4e\t%.4e\t%.4e\n' % (top1.avg, top5.avg,
                                                        losses.avg, batch_time.sum,
                                                        data_time.avg))


def validate(val_loader, model, criterion):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        target = target.cuda(async=True)
        input_var = torch.autograd.Variable(input, volatile=True)
        target_var = torch.autograd.Variable(target, volatile=True)

        # compute output
        output = model(input_var)
        loss = criterion(output, target_var)

        # measure accuracy and record loss
        prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
        losses.update(loss.data[0], input.size(0))
        top1.update(prec1[0], input.size(0))
        top5.update(prec5[0], input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Test: [{0}/{1}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses,
                   top1=top1, top5=top5))

    print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'
          .format(top1=top1, top5=top5))
    test_log.write('%.4e\t%.4e\n' % (top1.avg, top5.avg))

    return top1.avg


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    filename = os.path.join(checkpoint_dir, filename)
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.join(checkpoint_dir, 'model_best.pth.tar'))


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res


if __name__ == '__main__':
    main()
