import os
import math
import threading
import torch
import time
import numpy as np
import torch.cuda.comm as comm
from torch.autograd import Function
from torch.nn.modules import Module
from torch.nn.parallel.scatter_gather import scatter_kwargs, gather
from torch.nn.parallel.parallel_apply import parallel_apply

from .utils import AverageMeter, FrequencyMeter, Logger

__all__ = ['SparseDataParallel']


class SparseDataParallel(Module):
    """Implements data parallelism at the module level.
    It is the same as DataParallel but with sparse gradient.
    Args:
        module: module to be parallelized
        device_ids: CUDA devices (default: all devices)
        output_device: device location of output (default: device_ids[0])
        sparse_options: options for sparse backward
    """

    def __init__(self, module, device_ids=None, output_device=None, dim=0, **sparse_options):
        super(SparseDataParallel, self).__init__()
        if device_ids is None:
            device_ids = list(range(torch.cuda.device_count()))
        if output_device is None:
            output_device = device_ids[0]
        self.dim = dim
        self.module = module
        self.device_ids = device_ids
        self.output_device = output_device
        self.replicate = self._get_replicate_func(**sparse_options)
        if len(self.device_ids) == 1:
            self.module.cuda(device_ids[0])

    def forward(self, *inputs, **kwargs):
        inputs, kwargs = self.scatter(inputs, kwargs, self.device_ids)
        if len(self.device_ids) == 1:
            return self.module(*inputs[0], **kwargs[0])
        replicas = self.replicate(self.module, self.device_ids[:len(inputs)])
        outputs = self.parallel_apply(replicas, inputs, kwargs)
        return self.gather(outputs, self.output_device)

    def scatter(self, inputs, kwargs, device_ids):
        return scatter_kwargs(inputs, kwargs, device_ids, dim=self.dim)

    def parallel_apply(self, replicas, inputs, kwargs):
        return parallel_apply(replicas, inputs, kwargs)

    def gather(self, outputs, output_device):
        return gather(outputs, output_device, dim=self.dim)

    def _get_replicate_func(self, **sparse_options):
        self.sparse_broadcast = SparseBroadcast(self.device_ids, self.module, **sparse_options)

        def sparse_replicate(network, devices):
            devices = tuple(devices)
            num_replicas = len(devices)

            params = list(network.parameters())
            param_indices = {param: idx for idx, param in enumerate(params)}
            self.sparse_broadcast.set_target_gpus(devices)
            param_copies = self.sparse_broadcast(*params)
            if len(params) > 0:
                param_copies = [param_copies[i:i + len(params)]
                                for i in range(0, len(param_copies), len(params))]

            buffers = _buffers(network)
            buffer_indices = {buf: idx for idx, buf in enumerate(buffers)}
            buffer_copies = comm.broadcast_coalesced(buffers, devices)

            modules = list(network.modules())
            module_copies = [[] for device in devices]
            module_indices = {}

            for i, module in enumerate(modules):
                module_indices[module] = i
                for j in range(num_replicas):
                    replica = module.__new__(type(module))
                    replica.__dict__ = module.__dict__.copy()
                    replica._parameters = replica._parameters.copy()
                    replica._buffers = replica._buffers.copy()
                    replica._modules = replica._modules.copy()
                    module_copies[j].append(replica)

            for i, module in enumerate(modules):
                for key, child in module._modules.items():
                    module_idx = module_indices[child]
                    for j in range(num_replicas):
                        replica = module_copies[j][i]
                        replica._modules[key] = module_copies[j][module_idx]
                for key, param in module._parameters.items():
                    if param is None:
                        for j in range(num_replicas):
                            replica = module_copies[j][i]
                            replica._parameters[key] = None
                    else:
                        param_idx = param_indices[param]
                        for j in range(num_replicas):
                            replica = module_copies[j][i]
                            replica._parameters[key] = param_copies[j][param_idx]
                for key, buf in module._buffers.items():
                    if buf is None:
                        for j in range(num_replicas):
                            replica = module_copies[j][i]
                            replica._buffers[key] = None
                    else:
                        buffer_idx = buffer_indices[buf]
                        for j in range(num_replicas):
                            replica = module_copies[j][i]
                            replica._buffers[key] = buffer_copies[j][buffer_idx]

            return [module_copies[j][0] for j in range(num_replicas)]

        return sparse_replicate

    def print_frequency_log(self):
        for i in range(len(self.sparse_broadcast.frequency_meter.params)):
            for attr in self.sparse_broadcast.pruned_params_attrs:
                self.sparse_broadcast.frequency_log.write_buf('gpu.' + str(i) + '.' + attr[7] + '\n')
                for p in self.sparse_broadcast.frequency_meter.params[i][attr[0]]:
                    self.sparse_broadcast.frequency_log.write_buf('%d\t%d\t%d\t%d\n' % (p[0], p[1], p[2], p[3]))
        self.sparse_broadcast.frequency_log.write('over')

    def get_frequency_meter(self):
        return self.sparse_broadcast.frequency_meter

    def get_momentum_corrections(self):
        return self.sparse_broadcast.momentum_corrections

    def get_pruned_indices(self):
        return [attr[0] for attr in self.sparse_broadcast.pruned_params_attrs]

    def warm_up(self, epoch_or_iter):
        self.sparse_broadcast.warm_up(epoch_or_iter=epoch_or_iter)


def _buffers(network):
    buffers = []
    seen = set()
    for module in network.modules():
        for buf in module._buffers.values():
            if buf not in seen and buf is not None:
                seen.add(buf)
                buffers.append(buf)
    return buffers


class SparseBroadcast(Function):
    def __init__(self, target_gpus, module=None, **sparse_options):
        super(SparseBroadcast, self).__init__()
        self.target_gpus = target_gpus
        if not os.path.exists(sparse_options['log_dir']):
            os.makedirs(sparse_options['log_dir'])

        self.is_log_frequency = False
        self.is_log_sparsity = False
        self.frequency_meter = None
        self.sparsity_meter = None
        if sparse_options['is_log_frequency']:
            self.is_log_frequency = True
            self.frequency_meter = FrequencyMeter(len(target_gpus))
            self.frequency_log = Logger(os.path.join(sparse_options['log_dir'], 'communication.log'))
        if sparse_options['is_log_sparsity']:
            self.is_log_sparsity = True
            self.sparsity_meter = AverageMeter()
            self.sparsity_log = Logger(os.path.join(sparse_options['log_dir'], 'gradient.log'))
        
        self.sparse_func = self._get_sparse_func(module, **sparse_options)

    def set_target_gpus(self, devices):
        self.target_gpus = devices

    def forward(self, *inputs):
        if not all(input.is_cuda for input in inputs):
            raise TypeError('Broadcast function not implemented for CPU tensors')
        if len(inputs) == 0:
            return tuple()
        self.num_inputs = len(inputs)
        self.input_device = inputs[0].get_device()
        outputs = comm.broadcast_coalesced(inputs, self.target_gpus)
        return tuple([t for tensors in outputs for t in tensors])

    def backward(self, *grad_outputs):
        grad_outputs = self.sparse_func([grad_outputs[i:i + self.num_inputs]
                                         for i in range(0, len(grad_outputs), self.num_inputs)])
        out = comm.reduce_add_coalesced(grad_outputs, self.input_device)
        return out

    def warm_up(self, epoch_or_iter):
        if self.is_warm_up:
            if epoch_or_iter < self.warm_up_end:
                stage = epoch_or_iter // self.warm_up_step
                sparsity = 1 - self.warm_up_coeff ** (stage+1)
                if not self.sparsity == sparsity:
                    print('Sparse Data Parallel: set sparsity to %.3e' % sparsity)
                    self.sparsity = sparsity
            else:
                if not self.sparsity == self.final_sparsity:
                    print('Sparse Data Parallel: set sparsity to %.3e' % self.final_sparsity)
                    self.sparsity = self.final_sparsity

    def _get_sparse_func(self, module, **sparse_options):
        self.sparsity = sparse_options['sparsity']
        self.final_sparsity = self.sparsity
        algorithm = sparse_options['algorithm']
        is_prune_root_gpu = sparse_options['is_prune_root_gpu'] if 'is_prune_root_gpu' in sparse_options else True
        max_norm = sparse_options['max_norm'] if 'max_norm' in sparse_options else -1.0
        is_norm_local = sparse_options['is_norm_local'] if 'is_norm_local' in sparse_options else True
        if max_norm < 0.0:
            is_norm_local = False
        sample_rate = sparse_options['sample_rate'] if 'sample_rate' in sparse_options else -1.0
        is_sample = sparse_options['is_sample'] if 'is_sample' in sparse_options else (0.0 < sample_rate < 1.0)
        momentum = sparse_options['correction_coeff'] if 'correction_coeff' in sparse_options else -1.0
        nesterov = sparse_options['nesterov'] if 'nesterov' in sparse_options else False
        self.is_warm_up = sparse_options['is_warm_up'] if 'is_warm_up' in sparse_options else False
        self.warm_up_step = sparse_options['warm_up_step'] if 'warm_up_step' in sparse_options else 0
        self.warm_up_end = sparse_options['warm_up_end'] if 'warm_up_end' in sparse_options else 0
        assert (self.warm_up_end * self.warm_up_step) >= 0
        self.warm_up_step = math.fabs(self.warm_up_step)
        self.warm_up_end = int(math.fabs(self.warm_up_end))
        self.warm_up_coeff = sparse_options['warm_up_coeff'] if 'warm_up_coeff' in sparse_options else -1.0
        if self.warm_up_coeff <= 0 or self.warm_up_coeff >= 1 or self.warm_up_step == 0:
            self.is_warm_up = False

        print('Sparse DataParallel Config:\n'
              'sparsity: %.3f\n'
              'algorithm: %s\n'
              'is_prune_root_gpu: %s\n'
              'is_sample: %s\n'
              'sample_rate: %.3f\n'
              'is_norm_local: %s\n'
              'max_norm: %f\n'
              'momentum: %.2f\n'
              'nesterov: %s\n'
              'is_warm_up: %s\n'
              'warm_up_step: %f\n'
              'warm_up_end: %d\n'
              'warm_up_coeff: %.3e\n'
               % (self.sparsity, algorithm, str(is_prune_root_gpu),
                  str(is_sample), sample_rate,
                  str(is_norm_local), max_norm, momentum, str(nesterov),
                  str(self.is_warm_up), self.warm_up_step, self.warm_up_end,
                  self.warm_up_coeff
                  )
              )

        def get_num(base):
            def get_num_by_ratio(ratio):
                return int(math.ceil(base * ratio))
            return get_num_by_ratio

        self.pruned_params_attrs = []
        for idx, p in enumerate(module.named_parameters()):
            if 'weight' in p[0] and len(p[1].size()) > 1:
                numel = torch.numel(p[1].data)
                self.pruned_params_attrs.append((idx, numel, get_num(numel), p[0]))
                # math.ceil(numel * sparsity), 2
                # math.ceil(numel * (1.0-sparsity)), 3
                # math.ceil(numel * sample_rate), 4
                # math.ceil(numel * sample_rate * sparsity), 5
                # math.ceil(numel * sample_rate * (1.0-sparsity)), 6

                if self.is_log_frequency:
                    self.frequency_meter.register_param(idx, numel)

        def copy_and_zero(src):
            if isinstance(src, list) or isinstance(src, tuple):
                dst = []
                for s in src:
                    dst.append(copy_and_zero(s))
                return dst
            elif torch.is_tensor(src):
                return src.clone().zero_()

        if self.sparsity == 0:

            def identity(grad_params_on_gpus):
                if is_norm_local:
                    def _worker(grad_params, is_norm_local, max_norm):
                        if is_norm_local and max_norm > 0.0:
                            clip_grad_norm_local(grad_params, max_norm)

                    threads = [threading.Thread(target=_worker,
                                                args=(grad_params, is_norm_local, max_norm),
                                                )
                               for i, grad_params in enumerate(grad_params_on_gpus)]

                    for thread in threads:
                        thread.start()
                    for thread in threads:
                        thread.join()
                return grad_params_on_gpus

            return identity

        elif algorithm == 'accumulate':
            self.backup_grad_params_on_gpus = []

            def accumulate_sparse_func(grad_params_on_gpus):
                if self.is_log_frequency:
                    self.frequency_meter.tick_tock()
                if len(self.backup_grad_params_on_gpus) == 0:
                    self.backup_grad_params_on_gpus = copy_and_zero(grad_params_on_gpus)
                if self.is_log_sparsity:
                    self.sparsity_meter.reset()
                lock = threading.Lock()

                def _worker(i, grad_params, sparsity, pruned_params_attrs, backup_grad_params,
                            is_prune_root_gpu, root_device, is_norm_local, max_norm,
                            is_sample, sample_rate, sparsity_meter, frequency_meter, lock):

                    if is_norm_local and max_norm > 0.0:
                        clip_grad_norm_local(grad_params, max_norm)
                    if sparsity <= 0.0 or sparsity >= 1.0:
                        return
                    if i == root_device and not is_prune_root_gpu:
                        return

                    for attr in pruned_params_attrs:
                        grad_param = grad_params[attr[0]]
                        backup_grad_param = backup_grad_params[attr[0]]
                        backup_grad_param.add_(grad_param)
                        importance_abs = torch.abs(backup_grad_param)

                        if is_sample:
                            sample_indices = np.random.choice(attr[1], attr[2](sample_rate), replace=False)
                            sample_indices = torch.LongTensor(sample_indices).cuda(grad_param.get_device())
                            sampled_grad_param_abs = torch.abs(
                                importance_abs).view(attr[1]).index_select(0, sample_indices)
                            threshold = torch.max(torch.topk(sampled_grad_param_abs, attr[2](sample_rate * sparsity),
                                                             0, largest=False, sorted=False)[0])
                            mask = torch.gt(importance_abs, threshold)
                            grad_param.zero_().masked_copy_(mask, backup_grad_param[mask])
                            backup_grad_param.masked_fill_(mask, 0)
                            if self.is_log_frequency:
                                with lock:
                                    frequency_meter.update_by_mask(attr[0], i, mask.view(attr[1]), 1)
                        else:
                            if sparsity <= 0.5:
                                threshold = torch.max(torch.topk(importance_abs.view(attr[1]), attr[2](sparsity),
                                                                 0, largest=False, sorted=False)[0])
                                mask = torch.gt(importance_abs, threshold)
                                grad_param.zero_().masked_copy_(mask, backup_grad_param[mask])
                                backup_grad_param.masked_fill_(mask, 0)
                                if frequency_meter is not None:
                                    with lock:
                                        frequency_meter.update_by_mask(attr[0], i, mask.view(attr[1]), 1)
                            else:
                                _, topk_indices = torch.topk(importance_abs.view(attr[1]), attr[2](1.0-sparsity),
                                                             0, largest=True, sorted=False)
                                grad_param.zero_().view(attr[1]).index_copy_(0, topk_indices, backup_grad_param.view(
                                    attr[1]).index(topk_indices))
                                backup_grad_param.view(attr[1]).index_fill_(0, topk_indices, 0)
                                if frequency_meter is not None:
                                    with lock:
                                        frequency_meter.update_by_indices(attr[0], i, topk_indices)

                    if sparsity_meter is not None:
                        communication_sparsity = AverageMeter()
                        for grad_param in grad_params:
                            mask = torch.eq(grad_param, 0)
                            communication_sparsity.accumulate(mask.sum(), mask.numel())
                        with lock:
                            sparsity_meter.update(communication_sparsity.avg)

                threads = [threading.Thread(target=_worker,
                                            args=(i, grad_params, self.sparsity, self.pruned_params_attrs,
                                                  self.backup_grad_params_on_gpus[i],
                                                  is_prune_root_gpu, self.input_device, is_norm_local, max_norm,
                                                  is_sample, sample_rate, self.sparsity_meter, self.frequency_meter,
                                                  lock),
                                            )
                           for i, grad_params in enumerate(grad_params_on_gpus)]

                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                if self.is_log_sparsity:
                    self.sparsity_log.write('%f\n' % self.sparsity_meter.avg)
                return grad_params_on_gpus

            return accumulate_sparse_func

        elif algorithm == 'accumulate_momentum':
            self.backup_grad_params_on_gpus = []
            self.momentum_grad_params_on_gpus = []
            self.momentum_corrections_on_gpus = []
            self.momentum_corrections = None

            def accumulate_sparse_func(grad_params_on_gpus):
                if self.is_log_frequency:
                    self.frequency_meter.tick_tock()
                if len(self.backup_grad_params_on_gpus) == 0:
                    self.backup_grad_params_on_gpus = copy_and_zero(grad_params_on_gpus)
                    self.momentum_grad_params_on_gpus = copy_and_zero(grad_params_on_gpus)
                    self.momentum_corrections_on_gpus = copy_and_zero(grad_params_on_gpus)
                if self.is_log_sparsity:
                    self.sparsity_meter.reset()
                lock = threading.Lock()

                def _worker(i, grad_params, sparsity, pruned_params_attrs, backup_grad_params,
                            momentum_grad_params, momentum_corrections, momentum, nesterov,
                            is_prune_root_gpu, root_device, is_norm_local, max_norm,
                            is_sample, sample_rate, sparsity_meter, frequency_meter, lock):

                    if is_norm_local and max_norm > 0.0:
                        clip_grad_norm_local(grad_params, max_norm)
                    if sparsity <= 0.0 or sparsity >= 1.0:
                        for attr in pruned_params_attrs:
                            grad_param = grad_params[attr[0]]
                            momentum_correction = momentum_corrections[attr[0]]
                            momentum_correction.zero_().copy_(grad_param)
                        return
                    if i == root_device and not is_prune_root_gpu:
                        return

                    for attr in pruned_params_attrs:
                        grad_param = grad_params[attr[0]]
                        backup_grad_param = backup_grad_params[attr[0]]
                        momentum_grad_param = momentum_grad_params[attr[0]]
                        momentum_correction = momentum_corrections[attr[0]]
                        momentum_grad_param.add_(grad_param)
                        if nesterov:
                            backup_grad_param.add_(momentum, momentum_grad_param).add_(grad_param)
                        else:
                            backup_grad_param.add_(momentum_grad_param)
                        importance_abs = torch.abs(backup_grad_param)

                        if is_sample:
                            sample_indices = np.random.choice(attr[1], attr[2](sample_rate), replace=False)
                            sample_indices = torch.LongTensor(sample_indices).cuda(grad_param.get_device())
                            sampled_grad_param_abs = torch.abs(
                                importance_abs).view(attr[1]).index_select(0, sample_indices)
                            threshold = torch.max(torch.topk(sampled_grad_param_abs,
                                                             attr[2](sample_rate*sparsity), 0,
                                                             largest=False, sorted=False)[0])
                            mask = torch.gt(importance_abs, threshold)
                            grad_param.zero_().masked_copy_(mask, backup_grad_param[mask])
                            momentum_correction.zero_().masked_copy_(mask, momentum_grad_param[mask])
                            backup_grad_param.masked_fill_(mask, 0)
                            momentum_grad_param.masked_fill_(mask, 0)
                            if self.is_log_frequency:
                                with lock:
                                    frequency_meter.update_by_mask(attr[0], i, mask.view(attr[1]), 1)
                        else:
                            if sparsity <= 0.5:
                                threshold = torch.max(torch.topk(importance_abs.view(attr[1]), attr[2](sparsity),
                                                                 0, largest=False, sorted=False)[0])
                                mask = torch.gt(importance_abs, threshold)
                                grad_param.zero_().masked_copy_(mask, backup_grad_param[mask])
                                momentum_correction.zero_().masked_copy_(mask, momentum_grad_param[mask])
                                backup_grad_param.masked_fill_(mask, 0)
                                momentum_grad_param.masked_fill_(mask, 0)
                                if frequency_meter is not None:
                                    with lock:
                                        frequency_meter.update_by_mask(attr[0], i, mask.view(attr[1]), 1)
                            else:
                                _, topk_indices = torch.topk(importance_abs.view(attr[1]), attr[2](1.0-sparsity),
                                                             0, largest=True, sorted=False)
                                momentum_correction.zero_().view(attr[1]).index_copy_(0, topk_indices,
                                                      momentum_grad_param.view(attr[1]).index(topk_indices))
                                grad_param.zero_().view(attr[1]).index_copy_(0, topk_indices, backup_grad_param.view(
                                    attr[1]).index(topk_indices))
                                backup_grad_param.view(attr[1]).index_fill_(0, topk_indices, 0)
                                momentum_grad_param.view(attr[1]).index_fill_(0, topk_indices, 0)
                                if frequency_meter is not None:
                                    with lock:
                                        frequency_meter.update_by_indices(attr[0], i, topk_indices)
                        momentum_grad_param.mul_(momentum)

                    if sparsity_meter is not None:
                        communication_sparsity = AverageMeter()
                        for grad_param in grad_params:
                            mask = torch.eq(grad_param, 0)
                            communication_sparsity.accumulate(mask.sum(), mask.numel())
                        with lock:
                            sparsity_meter.update(communication_sparsity.avg)

                threads = [threading.Thread(target=_worker,
                                            args=(i, grad_params, self.sparsity, self.pruned_params_attrs,
                                                  self.backup_grad_params_on_gpus[i],
                                                  self.momentum_grad_params_on_gpus[i],
                                                  self.momentum_corrections_on_gpus[i],
                                                  momentum, nesterov,
                                                  is_prune_root_gpu, self.input_device,
                                                  is_norm_local, max_norm, is_sample, sample_rate,
                                                  self.sparsity_meter, self.frequency_meter, lock),
                                            )
                           for i, grad_params in enumerate(grad_params_on_gpus)]

                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                self.momentum_corrections = comm.reduce_add_coalesced(self.momentum_corrections_on_gpus,
                                                                      self.input_device)
                if self.is_log_sparsity:
                    self.sparsity_log.write('%f\n' % self.sparsity_meter.avg)
                return grad_params_on_gpus

            return accumulate_sparse_func

        else:
            assert False


def clip_grad_norm(parameters, max_norm, norm_type=2):
    """Clips gradient norm of an iterable of parameters.

    The norm is computed over all gradients together, as if they were
    concatenated into a single vector. Gradients are modified in-place.

    Arguments:
        parameters (Iterable[Tensor]): an iterable of Tensors that will have
            gradients normalized
        max_norm (float or int): max norm of the gradients
        norm_type (float or int): type of the used p-norm. Can be ``'inf'`` for infinity norm.
    """
    max_norm = float(max_norm)
    norm_type = float(norm_type)
    if norm_type == float('inf'):
        total_norm = max(p.abs().max() for p in parameters)
    else:
        total_norm = 0
        for p in parameters:
            param_norm = p.norm(norm_type)
            total_norm += param_norm ** norm_type
        total_norm = total_norm ** (1. / norm_type)
    clip_coef = max_norm / (total_norm + 1e-6)
    if clip_coef < 1:
        for p in parameters:
            p.mul_(clip_coef)


def clip_grad_norm_local(parameters, max_norm, norm_type=2):
    """Clips gradient norm of an iterable of parameters.

    The norm is computed over all gradients together, as if they were
    concatenated into a single vector. Gradients are modified in-place.

    Arguments:
        parameters (Iterable[Tensor]): an iterable of Tensors that will have
            gradients normalized
        max_norm (float or int): max norm of the gradients
        norm_type (float or int): type of the used p-norm. Can be ``'inf'`` for infinity norm.
    """
    max_norm = float(max_norm)
    norm_type = float(norm_type)
    if norm_type == float('inf'):
        total_norm = max(p.abs().max() for p in parameters)
        clip_coef = max_norm / (total_norm + 1e-6)
        if clip_coef < 1:
            for p in parameters:
                p.mul_(clip_coef)
    else:
        for p in parameters:
            param_norm = p.norm(norm_type)
            total_norm = param_norm * len(parameters)
            clip_coef = max_norm / (total_norm + 1e-6)
            if clip_coef < 1:
                p.mul_(clip_coef)