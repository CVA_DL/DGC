import torch
from collections import defaultdict
from torch.optim.optimizer import Optimizer, required

__all__ = ['SparseSGD']


class SparseSGD(Optimizer):
    r"""Implements stochastic gradient descent (optionally with momentum).
    Nesterov momentum is based on the formula from
    `On the importance of initialization and momentum in deep learning`__.
    Args:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float): learning rate
        momentum (float, optional): momentum factor (default: 0)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
    """

    def __init__(self, params, lr=required, momentum=0, weight_decay=0, nesterov=False):
        defaults = dict(lr=lr, momentum=momentum, weight_decay=weight_decay, nesterov=nesterov)
        super(SparseSGD, self).__init__(params, defaults)
        self.frequency_meter = None
        self.momentum_corrections = None
        self.pruned_indices = None

    def __setstate__(self, state):
        super(SparseSGD, self).__setstate__(state)

    def set_frequency_meter(self, frequency_meter):
        self.frequency_meter = frequency_meter

    def set_momentum_corrections(self, momentum_corrections):
        self.momentum_corrections = momentum_corrections

    def set_pruned_indices(self, pruned_indices):
        self.pruned_indices = pruned_indices

    def step(self, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            nesterov = group['nesterov']

            for idx, p in enumerate(group['params']):
                if p.grad is None:
                    continue
                d_p = p.grad.data
                if self.frequency_meter is not None:
                    mask = torch.ne(d_p, 0).float()
                    self.frequency_meter.update_by_mask(idx, self.frequency_meter.nGPUs, mask.view(mask.numel()))
                if self.momentum_corrections is None or (idx not in self.pruned_indices):
                    if weight_decay != 0:
                        d_p.add_(weight_decay, p.data)
                    if momentum != 0:
                        param_state = self.state[p]
                        if 'momentum_buffer' not in param_state:
                            buf = param_state['momentum_buffer'] = d_p.clone()
                        else:
                            buf = param_state['momentum_buffer']
                            buf.mul_(momentum).add_(d_p)
                        if nesterov:
                            d_p = d_p.add(momentum, buf)
                        else:
                            d_p = buf
                else:
                    momentum_correction = self.momentum_corrections[idx]
                    if weight_decay != 0:
                        momentum_correction.add_(weight_decay, p.data)
                        d_p.add_(weight_decay, p.data)
                    if momentum != 0:
                        param_state = self.state[p]
                        if 'momentum_buffer' not in param_state:
                            param_state['momentum_buffer'] = momentum_correction.clone()
                            if nesterov and weight_decay != 0:
                                d_p.add_(momentum, torch.mul(p.data, weight_decay))
                        else:
                            buf = param_state['momentum_buffer']
                            buf.mul_(momentum)
                            if nesterov:
                                if weight_decay != 0:
                                    d_p.add_(momentum, torch.add(buf, weight_decay, p.data))
                                else:
                                    d_p.add_(momentum, buf)
                            else:
                                d_p = buf.clone().add_(d_p)
                            buf.add_(momentum_correction)
                p.data.add_(-group['lr'], d_p)

        return loss
