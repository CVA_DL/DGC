import os
import numpy as np
import datetime

__all__ = ['AverageMeter', 'FrequencyMeter', 'Logger', 'get_alg_id', 'gradient_stats']


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def accumulate(self, val, n=1):
        self.sum += val
        self.count += n
        self.avg = self.sum / self.count


class FrequencyMeter(object):
    """Record the update frequency of Parameters"""
    def __init__(self, nGPUs):
        self.reset(nGPUs)

    def reset(self, nGPUs):
        self.nGPUs = nGPUs
        self.params = [dict() for i in range(nGPUs+1)]
        self.tick = 0

    def tick_tock(self):
        self.tick += 1

    def register_param(self, param_idx, size):
        # last_tick, count, sum, max
        for p in self.params:
         p[param_idx] = [[0, 0, 0, 0] for i in range(size)]

    def update_by_mask(self, param_idx, gpu_id, mask_flatten, val=1):
        if param_idx in self.params[0]:
            for idx, v in enumerate(mask_flatten):
                if v == val:
                    last_tick = self.params[gpu_id][param_idx][idx][0]
                    duration = self.tick - last_tick
                    self.params[gpu_id][param_idx][idx][1] += 1
                    self.params[gpu_id][param_idx][idx][2] += duration
                    self.params[gpu_id][param_idx][idx][3] = max(self.params[gpu_id][param_idx][idx][3], duration)
                    self.params[gpu_id][param_idx][idx][0] = self.tick

    def update_by_indices(self, param_idx, gpu_id, indices):
        if param_idx in self.params[0]:
            for idx in indices:
                last_tick = self.params[gpu_id][param_idx][idx][0]
                duration = self.tick - last_tick
                self.params[gpu_id][param_idx][idx][1] += 1
                self.params[gpu_id][param_idx][idx][2] += duration
                self.params[gpu_id][param_idx][idx][3] = max(self.params[gpu_id][param_idx][idx][3], duration)
                self.params[gpu_id][param_idx][idx][0] = self.tick


class Logger(object):
    """Write log immediately to the disk"""
    def __init__(self, filepath):
        self.f = open(filepath, 'w')
        self.fid = self.f.fileno()
        self.filepath = filepath

    def close(self):
        self.f.close()

    def write(self, content):
        self.f.write(content)
        self.f.flush()
        os.fsync(self.fid)

    def write_buf(self, content):
        self.f.write(content)


def get_alg_id(**sparse_options):
    arch = sparse_options['arch']
    sparsity = sparse_options['sparsity']
    algorithm = sparse_options['algorithm']
    is_prune_root_gpu = sparse_options['is_prune_root_gpu'] if 'is_prune_root_gpu' in sparse_options else True
    max_norm = sparse_options['max_norm'] if 'max_norm' in sparse_options else -1.0
    is_norm_local = sparse_options['is_norm_local'] if 'is_norm_local' in sparse_options else True
    if max_norm < 0.0:
        is_norm_local = False
    sample_rate = sparse_options['sample_rate'] if 'sample_rate' in sparse_options else -1.0
    is_sample = sparse_options['is_sample'] if 'is_sample' in sparse_options else (0.0 < sample_rate < 1.0)
    correction_coeff = sparse_options['correction_coeff'] if 'correction_coeff' in sparse_options else -1.0
    nesterov = sparse_options['nesterov'] if 'nesterov' in sparse_options else False
    learning_anneal = sparse_options['learning_anneal'] if 'learning_anneal' in sparse_options else -1.0
    is_warm_up = sparse_options['is_warm_up'] if 'is_warm_up' in sparse_options else False
    warm_up_step = sparse_options['warm_up_step'] if 'warm_up_step' in sparse_options else 0
    warm_up_end = sparse_options['warm_up_end'] if 'warm_up_end' in sparse_options else 0
    assert (warm_up_end*warm_up_step) >= 0
    warm_up_coeff = sparse_options['warm_up_coeff'] if 'warm_up_coeff' in sparse_options else -1.0
    if warm_up_coeff < 0 or warm_up_coeff >= 1 or warm_up_coeff <= 0:
        is_warm_up = False

    config = 'Training Config:\n' \
             'arch: %s\n' \
             'learning_rate: %.3f\n' \
             'momentum: %.3f\n' \
             'sparsity: %.3f\n' \
             'algorithm: %s\n' \
             'is_prune_root_gpu: %s\n' \
             'is_sample: %s\n' \
             'sample_rate: %.3f\n' \
             'is_norm_local: %s\n' \
             'max_norm: %f\n' \
             'correction_coeff: %.3f\n' \
             'nesterov: %s\n' \
             'is_warm_up: %s\n' \
             'warm_up_step: %d\n' \
             'warm_up_end: %d\n' \
             'warm_up_coeff: %.3e\n' \
             'learning_anneal: %.3e\n' \
             % (arch, sparse_options['lr'], sparse_options['momentum'],
                sparsity, algorithm, str(is_prune_root_gpu), str(is_sample), sample_rate,
                str(is_norm_local), max_norm, correction_coeff, str(nesterov),
                str(is_warm_up), warm_up_step, warm_up_end, warm_up_coeff, learning_anneal)

    print(config)

    alg_id = '0s'
    if sparsity > 0:
        if algorithm == 'accumulate':
            alg_id = 'ac'
        elif algorithm == 'accumulate_momentum':
            if nesterov:
                alg_id = 'an'
            else:
                alg_id = 'am'
    alg_id = arch + '_' + alg_id
    if is_warm_up:
        alg_id += '_w'
    else:
        alg_id += '_c'
    if is_sample:
        alg_id += '_s'
    else:
        alg_id += '_g'
    if is_prune_root_gpu:
        alg_id += '_all'
    else:
        alg_id += '_not'
    if is_norm_local:
        alg_id += '_ln'
    else:
        alg_id += '_gn'
    alg_id += '_sp%.3f_%s' % (sparsity, datetime.datetime.now().strftime('%m%d_%H%M'))

    print('saving to %s' % alg_id)

    return alg_id, config


def gradient_stats(model, filepath='stats.log'):
    logger = Logger(filepath=filepath)
    for idx, (name, param) in enumerate(model.named_parameters()):
        logger.write('@Param:' + name + '\n')
        if param.grad is None:
            logger.write('------ skipping ------\n')
        else:
            logger.write('------ stating ------\n')
            for val in param.grad.data.view(param.grad.data.numel()):
                logger.write_buf('%.6e\n' % val)
    logger.close()


class HistMeter(object):
    def __init__(self):
        self.x = []
        self.y = []

    def update(self, x, y,):
        self.x.append(x)
        self.y.append(y)

    def get_hist(self, bins):
        H, x_edges, y_edges = np.histogram2d(np.ndarray(self.x), np.ndarray(self.y), bins)
        H = H.tolist()
        x_edges = x_edges.tolist()
        y_edges = y_edges.tolist()
        return H, x_edges, y_edges


def preprocess_gradient_stats_log(dir_path, hist=False, name=None):
    log_list = os.listdir(dir_path)
    epochs = list(map(lambda x: int(x.split('.')[0].split('_')[-1]), log_list))
    hist_meters = dict()
    for idx, filename in enumerate(log_list):
        epoch = epochs[idx]
        is_stat = False
        with open(os.path.join(dir_path, filename)) as f:
            while True:
                tline = f.readline()
                if not tline:
                    break
                try:
                    val = float(tline)
                    if is_stat:
                        hist_meters[grad_name].update(val, epoch)
                except ValueError:
                    if tline[0] == '@':
                        grad_name = tline[7:]
                        tline = f.readline()
                        if 'stating' in tline and (name is None or grad_name in name):
                            is_stat = True
                            hist_meters[grad_name] = HistMeter()
                        else:
                            is_stat = False
    for grad_name, hist_meter in hist_meters.items():
        if hist:
            H, x_edges, y_edges = hist_meter.get_hist([100, len(epochs)])
            with open(os.path.join(dir_path, grad_name+'.stat'), 'w') as f:
                f.write('Hist\n')
                for x in H:
                    for y in x:
                        f.write(str(y) + '\n')
                f.write('Xedge\n')
                for x in x_edges:
                    f.write(str(x) + '\n')
                f.write('Yedge\n')
                for y in y_edges:
                    f.write(str(y) + '\n')
        else:
            with open(os.path.join(dir_path, grad_name + '.stat'), 'w') as f:
                for idx, x in enumerate(hist_meter.x):
                    f.write('%.6e\t%.6e\n' % (x, hist_meter.y[idx]))
